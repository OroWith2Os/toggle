app_conf = configuration_data()
app_conf.set('profile', profile)
app_conf.set('prefix', prefix)
app_conf.set('libdir', libdir)
app_conf.set('pkgdatadir', pkgdatadir)
app_conf.set('PACKAGE_VERSION', meson.project_version())
app_conf.set('PACKAGE_NAME', meson.project_name())
app_conf.set('GJS', gjs_console)
app_conf.set('APPLICATION_ID', application_id)

app = configure_file(
  input: 'app.drey.Toggle.in',
  output: application_id,
  install: true,
  install_dir: pkgdatadir,
  configuration: app_conf
)

sources = files(
  'components/font-selector.ts',
  'pages/appearance.ts',
  'pages/desktop.ts',
  'pages/devices.ts',
  'pages/startup-apps.ts',
  'pages/util.ts',
  'utils/pango.util.ts',
  'application.ts',
  'main.ts',
  'preferences.ts',
  'warning.ts',
  'window.ts',
)

tsc_out = meson.project_build_root() / 'tsc-out'

typescript = custom_target(
  'typescript-compile',
  input: sources,
  build_by_default: true,
  build_always_stale: true,
  command: [ tsc, '--outDir', tsc_out ],
  output: ['tsc-output'],
)

source_res_conf = configuration_data()
source_res_conf.set('profile', profile)
src_res = gnome.compile_resources(
  application_id + '.src',
  configure_file(
  	input: 'app.drey.Toggle.src.gresource.xml.in',
  	output: '@BASENAME@',
  	configuration: source_res_conf
  ),
  dependencies: typescript,
  source_dir: tsc_out,
  gresource_bundle: true,
  install: true,
  install_dir: pkgdatadir
)

run_target('run',
  command: app,
  depends: [
    data_res,
    src_res
  ]
)
