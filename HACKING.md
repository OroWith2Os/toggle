# Getting Started

## Prerequisites

To get started with contributing to Toggle, you'll want to install the following Flatpak packages:

- `org.gnome.Platform//master`
- `org.gnome.Sdk//master`
- `org.freedesktop.Sdk.Extension.node18//23.08`
- `org.freedesktop.Sdk.Extension.typescript//23.08`

Note that if you do not install those packages, `flatpak-builder` will demand that you install the `master` version of the `node18` and `typescript` packages.

You will need to instead install the latest available runtime for those packages. Please refer to [this open issue](https://github.com/flatpak/flatpak-builder/issues/469) for more information.

## Cloning

When cloning the repository, make sure to add the `--recursive` parameter to your `git clone` command.

If you've already cloned the repository, you can use `git submodule update --init --recursive` to clone the sub-modules.

This will ensure all sub-modules are properly cloned, and that you're ready to build.

## Using GNOME Builder

The easiest way to get started with contributing to Toggle is to use [GNOME Builder](flathub.org/apps/org.gnome.Builder).

If Builder doesn't do it for you automatically, you need to enable the following Flatpak remotes:

- [gnome-nightly](https://wiki.gnome.org/Apps/Nightly)
- [flathub](flatpak.org/setup)

## Using VS Code or Another Code Editor

You can also contribute to Toggle using the code editor of your choice.

If you are using VS Code, the following extensions are recommended for development:

- [GTK Blueprint by Bodil Stokke](https://marketplace.visualstudio.com/items?itemName=bodil.blueprint-gtk)
- [Flatpak by Bilal Elmoussaoui](https://marketplace.visualstudio.com/items?itemName=bilelmoussaoui.flatpak-vscode)
- [Meson](https://marketplace.visualstudio.com/items?itemName=mesonbuild.mesonbuild)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

To build Toggle, run the following command from the project's root directory:

`flatpak-builder --force-clean build ./build-aux/app.drey.Toggle.Devel.json`

If you wish to install and run the package in the user's Flatpak environment, include the following flags:

- `--user`
- `--install`

You can now run the app using one of four methods:

### If Toggle is installed

Run `flatpak run app.drey.Toggle.Devel` or find Toggle in your application list.

### If you're using VS Code with the Flatpak extension installed

Click on the ▶️ button in the top right of your code window.

### If you'd like to generate a `.flatpak` bundle

Re-run the build command with the `--repo=repo` flag, followed by:

`flatpak build-bundle repo app.drey.Toggle.Devel.flatpak app.drey.Toggle.Devel master`

This will generate a flatpak bundle file named `app.drey.Toggle.Devel.flatpak` at the root of the project.

# Contributing to the Project

Thank you for contributing to Toggle! Please note the following before you contribute:

## Code of Conduct (CoC)

Before contributing, please familiarize yourself with the Contributor Covenant, located [here](./CODE_OF_CONDUCT.md).

## Task Collaboration Guide

Collaboration is vital to making great software. Before engaging in a task:

1. Check if the task you'd like to complete is listed as a To-Do item in the [README.md](./README.md)
2. If your task is not listed as a To-Do item, [join us on our Matrix channel](https://matrix.to/#/#toggle-application:fedora.im) and discuss your proposal with us!
3. After receiving approval, please create a new task on the [Kanban, or "Issue Boards" section](https://gitlab.com/OroWith2Os/toggle/-/boards) so we know what you're working on.<br/>
   Label it as "WIP", assign it to yourself, and optionally, add a body.<br/>
   ![Open an issue in the WIP column](./docs/create-issue.png)
   ![Assign it to yourself in the right panel](./docs/assign-issue.png)

4. Open a Merge Request (MR), and drag and drop your task into the "MR Submitted" column. Flag us down in the channel with a link to your MR for reviews.
5. Get your MR merged.

Congratulations! You just collaborated to Toggle.

This process helps prevent overlapping or redundant efforts.
